class Question {
    constructor(element) {
        this.formData = new FormData(element.querySelector('.data-form'));
        this.id = element.getAttribute('data');
        this.likeButton = element.querySelector('.like-q');
        this.dislikeButton = element.querySelector('.dislike-q');
        this.likeCount = element.querySelector('.like-c');

        this.likeButton.onclick = () => this.like();
        this.dislikeButton.onclick = () => this.dislike();
    }

    setLikeCount(likeCount) {
        this.likeCount.innerHTML = likeCount;
    }

    like() {
        fetch('/api/like/', {
            method: 'POST',
            body: this.formData
        }).then(response => {
            if (response.ok) {
                response.json().then(data => this.setLikeCount(data.total_likes));
            }
        });
    }

    dislike() {
        fetch('/api/dislike/', {
            method: 'POST',
            body: this.formData
        }).then(response => {
            if (response.ok) {
                response.json().then(data => this.setLikeCount(data.total_likes));
            }
        });
    }
}

const questions = document.querySelectorAll('.question-q');

let questionObjs = [];
for (let i = 0; i < questions.length; i++) {
    questionObjs.push(new Question(questions[i]));
}
