from django.urls import path
from forum.views import *
from django.conf import settings as django_settings
from django.conf.urls.static import static

urlpatterns = [
    path('', index, name='about'),
    path('ask', ask, name='ask'),
    path('<int:question_id>', question, name='question'),
    path('<int:tag_id>', tag, name='tag'),
    path('settings', settings_view, name='settings'),
    path('login', login_view, name='login'),
    path('signup', signup, name='signup'),
    path('logout', logout_view, name='logout'),
    path('hot', hot, name='hot'),
    path('api/like/', like),
    path('api/dislike/', dislike),
] + static(django_settings.MEDIA_URL, document_root=django_settings.MEDIA_ROOT)
