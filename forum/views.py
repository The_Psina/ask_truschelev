from django.db import IntegrityError
from django.shortcuts import render
from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.template import loader
from django.views.generic import TemplateView
from .models import *
from django.contrib.auth.decorators import permission_required, login_required
from django.core.paginator import Paginator
from django.db.models import Count
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def paginate(objects, request, amount):
    paginator = Paginator(objects, amount)
    page = request.GET.get('page')
    objects_page = paginator.get_page(page)
    return objects_page


def render_question(request, file, objects):
    template = loader.get_template(file)
    context = {
        'questions': paginate(objects, request, 5),
    }
    return HttpResponse(template.render(context, request))


def question(request, question_id):
    if request.method == 'POST':
        form = AnswerForm(data=request.POST)
        if form.is_valid:
            answer = form.save(commit=False)
            answer.author = request.user
            answer.question = Question.objects.get(id=question_id)
            answer.save()
            return redirect('question', question_id)

    form = AnswerForm()
    answers = Answer.objects.answer(question_id)
    template = loader.get_template('question.html')
    context = {
        'form': form,
        'answers': paginate(answers, request, 5),
        'question': Question.objects.get(id=question_id)
    }
    return HttpResponse(template.render(context, request))


def tag(request, tag_id):
    latest_question_list = Question.objects.tag(tag_id)
    template = loader.get_template('tag.html')
    context = {
        'questions': paginate(latest_question_list, request, 5),
        'tag': Tag.objects.get(id=tag_id)
    }
    return HttpResponse(template.render(context, request))


@login_required(login_url='login')
def like(request):
    question = Question.objects.get(pk=request.POST['id'])
    new_like = Like(question=question, user=request.user)
    try:
        dislike = question.dislikes.filter(user=request.user)
        if dislike:
            dislike.delete()
        new_like.save()
        question.rating = question.likes.count() - question.dislikes.count()
        question.save()
    except IntegrityError as e:
        question.likes.filter(user=request.user).delete()
    return JsonResponse({'total_likes': question.total_likes})


@login_required(login_url='login')
def dislike(request):
    question = Question.objects.get(pk=request.POST['id'])
    new_dislike = Dislike(question=question, user=request.user)
    try:
        like = question.likes.filter(user=request.user)
        if like:
            like.delete()
        new_dislike.save()
        question.rating = question.likes.count() - question.dislikes.count()
        question.save()
    except IntegrityError as e:
        question.dislikes.filter(user=request.user).delete()
    return JsonResponse({'total_likes': question.total_likes})


def index(request):
    latest_question_list = Question.objects.new()
    return render_question(request, 'index.html', latest_question_list)


def hot(request):
    latest_question_list = Question.objects.all().order_by('rating')[::-1]
    return render_question(request, 'hot.html', latest_question_list)


@login_required(login_url='login')
def ask(request):
    if request.method == 'POST':
        form = AskForm(data=request.POST)
        if form.is_valid:
            question = form.save(commit=False)
            question.author = request.user
            question.save()
            for tag in form['tag_list'].data.split():
                new_tag = Tag.objects.get_or_create(name=tag)
                print(new_tag)
                question.tag.add(new_tag[0])
            question.save()
            return redirect('question', question.id)

    if request.method == 'GET':
        form = AskForm()
    return render(request, 'ask.html', {'form': form})


@login_required(login_url='login')
def settings_view(request):
    if request.method == 'POST':
        form = SettingsForm(data=request.POST, files=request.FILES, instance=request.user)
        if form.is_valid:
            user = form.save(commit=False)
            user.save()
        return render(request, 'settings.html', {'form': form})
    if request.method == 'GET':
        form = SettingsForm(instance=request.user)
    return render(request, 'settings.html', {'form': form})


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST, files=request.FILES)
        print(form.errors)
        if form.is_valid():
            user = form.get_user()
            if user is not None:
                login(request, user)
                return redirect('about')
    if request.method == 'GET':
        form = AuthenticationForm()
    return render(request, 'login.html', {'form': form})


def logout_view(request):
    logout(request)
    return redirect('login')


def signup(request):
    if request.method == 'POST':
        form = SignupForm(data=request.POST, files=request.FILES)
        print(form.errors)
        if form.is_valid():
            print("valid")
            user = form.save(commit=False)
            user.set_password(user.password)
            user.save()
            return redirect('login')

    form = SignupForm()
    context = {
        'form': form
    }
    return render(request, 'signup.html', context)
# Create your views here.
