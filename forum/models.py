from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.utils import timezone
from django import forms
from django.shortcuts import get_object_or_404


class ModelManager(models.Manager):
    def top(self):
        return self.order_by('rating')[::-1]

    def tag(self, tag_id):
        tags = get_object_or_404(Tag, pk=int(tag_id))
        return self.filter(tag__id=tags.id)

    def answer(self, question_id):
        question = get_object_or_404(Question, pk=int(question_id))
        return self.filter(question__id=question.id)

    def new(self):
        return self.order_by()[::-1]


class Tag(models.Model):
    name = models.CharField(max_length=200, unique=True)


class Profile(AbstractUser):
    nickname = models.CharField(max_length=200)
    avatar_path = models.ImageField(upload_to="", default="ymlql7_SavanB2.jpg")


class Question(models.Model):
    objects = ModelManager()
    title = models.CharField(max_length=200)
    question_text = models.TextField()
    author = models.ForeignKey(Profile, on_delete=models.CASCADE)
    tag = models.ManyToManyField(Tag)
    create_at = models.DateTimeField(default=timezone.now)
    rating = models.SmallIntegerField(default=0)

    @property
    def total_likes(self):
        return self.likes.count() - self.dislikes.count()

    @property
    def get_avatar(self):
        return self.author.avatar_path

    class Meta:
        ordering = ['-create_at']


class Answer(models.Model):
    objects = ModelManager()
    comment = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answer')
    author = models.ForeignKey(Profile, on_delete=models.CASCADE, related_name='author')
    create_at = models.DateTimeField(default=timezone.now)
    rating = models.SmallIntegerField(default=0)

    @property
    def get_avatar(self):
        return self.author.avatar_path

    class Meta:
        ordering = ['-create_at']


class SignupForm(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Profile
        fields = ('username', 'email', 'nickname', 'password', 'avatar_path',)

    def clean(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('confirm_password')
        print(password1)
        print(password2)
        if password1 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")

        return self.cleaned_data


class AskForm(forms.ModelForm):
    tag_list = forms.CharField(max_length=500, initial="", required=True)

    class Meta:
        model = Question
        fields = ('title', 'question_text')


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ('comment', )


class SettingsForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('username', 'email', 'nickname', 'avatar_path',)


class Like(models.Model):
    question = models.ForeignKey(Question, related_name="likes", on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, related_name="question_likes", on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('user', 'question')


class Dislike(models.Model):
    question = models.ForeignKey(Question, related_name="dislikes", on_delete=models.CASCADE)
    user = models.ForeignKey(Profile, related_name="question_dislikes", on_delete=models.CASCADE)
    created_at = models.DateTimeField(default=timezone.now)

    class Meta:
        unique_together = ('user', 'question')
