from django.core.management.base import BaseCommand
from django.db import IntegrityError
from django.contrib.auth.models import User
from faker import Faker
import random
from forum.models import Question, Tag, Profile, Answer, Like, Dislike

USER_COUNT = 20
TAG_COUNT = 10
QUESTION_COUNT = 40
ANSWER_COUNT = 80


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.create_tags()
        self.create_profiles()
        tags = Tag.objects.all()
        profiles = Profile.objects.all()
        self.create_questions(profiles, tags)
        questions = Question.objects.all()
        self.add_tag_question_relations(questions=questions, tags=tags)
        self.create_answers(profiles, questions)
        answers = Answer.objects.all()

    def create_users(self):
        users = []
        faker = Faker()
        for i in range(USER_COUNT):
            try:
                user = User.objects.create_user(username=faker.user_name(),
                                                email=faker.email())
                user.set_password(faker.name())
                user.save()
                users.append(user)
            except IntegrityError:
                pass

    def create_profiles(self):
        profiles = []
        faker = Faker()
        for i in range(USER_COUNT):
            profile = Profile(username=faker.user_name(), nickname=faker.user_name())
            profile.set_password(faker.first_name())
            profiles.append(profile)

        Profile.objects.bulk_create(profiles)

    def create_tags(self):
        tags = []
        faker = Faker()
        for i in range(TAG_COUNT):
            tag = Tag(name=faker.word() + str(i))
            tags.append(tag)

        Tag.objects.bulk_create(tags, batch_size=500)

    def create_questions(self, profiles, tags):
        questions = []
        faker = Faker()
        for _ in range(QUESTION_COUNT):
            question = Question(title=faker.sentence()[:random.randint(10, 130)],
                                question_text=faker.text(),
                                author=random.choice(profiles))
            questions.append(question)
        Question.objects.bulk_create(questions, batch_size=500)

    def create_answers(self, profiles, questions):
        answers = []
        faker = Faker()
        for _ in range(ANSWER_COUNT):
            answer = Answer(author=random.choice(profiles),
                            question=random.choice(questions),
                            comment=faker.text()
                            )
            answers.append(answer)
        Answer.objects.bulk_create(answers, batch_size=500)

    def add_tag_question_relations(self, questions, tags):
        for question in questions:
            tags_for_questions = []
            for _ in range(random.randint(1, 3)):
                tag = random.choice(tags)
                if tag.pk not in tags_for_questions:
                    if question.tag.count() < 4:
                        tags_for_questions.append(tag.pk)
            question.tag.add(*tags_for_questions)
